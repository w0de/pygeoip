from setuptools import setup, find_packages

print 'setuptools...'

setup(
    name='pygeoip',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'pickledb',
        'pyipinfoio',
        'pycountry',
        'pendulum'
        'netaddr'
    ],
    package_data={
        'geoip': ['*.db']
    },
    author='w0de',
    description='''
    API for python; ip -> country & country -> ips. No ipv6 support (yet).
    Region support and cli also planned.
    Database provided by DB-IP.com (thanks!).
    ''',
    url='bitbucket.org/w0de/geoip'
)

from geoip import updater
updater.cli()
