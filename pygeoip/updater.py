#!/usr/bin/env python

import sys
from pendulum import now
from os.path import abspath, isfile, dirname, join
from netaddr import iprange_to_cidrs as calc_cidr

DIRNAME = abspath(dirname(__file__))

def current_url():
    n = now()
    y, m = str(n.year), str(n.month)
    m = m if len(m) > 1 else '0' + m
    return 'http://download.db-ip.com/free/dbip-country-{0}-{1}.csv.gz'.format(y, m)

def dl_csv():
    filename = join(DIRNAME, 'dbip-country.csv')
    with open(filename, 'wb') as f:
        r = requests.get(current_url())
        f.write(r.content)
    return filename

def read_row(row):
    try:
        ctry = row[2]
        cidr = calc_cidr(row[0], row[1])
        assert len(ctry) == 2
        return ctry, cidr
    except:
        return None, None

def cli():
    print 'updating ip database from DB-IP.com...'
    main()

def main():
    csv = dl_csv()
    db = pickledb.load(join(DIRNAME, 'ip.db'), False)
    with open(csv, 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            country, cidr = read_row(row)
            if country and cidr:
                db.append(country, cidr)
    db.dump()
    sys.exit()

if __name__ == '__main__':
    main()
