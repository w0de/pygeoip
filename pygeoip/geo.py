import pickledb
import pyipinfoio
from pycountry import countries
from pycountry.db import Country
from collections import namedtuple
from os.path import dirname, abspath, join
from netaddr import IPAddress, IPNetwork, IPSet

GEOIP_TUPLE = namedtuple('geoip_country', ['country', 'ipset', 'size'])
IPINFO_IO = pyipinfoio.IPLookup()
DB_FILE = join(abspath(dirname(__file__)), 'ip.db')
DB = pickledb.load(db_file, False)

def get(country):
    if not isinstance(country, Country):
        try:
            country = countries.lookup(country)
        except LookupError as e:
            return None
    raw = DB.get(country.alpha_2.upper())
    net = map(IPNetwork, raw)
    ipset = IPSet(*net)
    return GEOIP_TUPLE(country, ipset, ipset.size)

def ipinfo_io(ip):
    try:
        return IPINFO_IO(ip, 'country')
    except:
        return None

def db_value_search(ip):
    matches = []
    for country_code in DB.getall():
        geoip_tuple = get(country_code)
        if ip in geoip_tuple.ipset:
            matches.append(geoip_tuple)
    return matches

def from_ip(ip, ipinfo=True):
    ip = IPAddress(ip)
    if ipinfo is True:
        country_code = ipinfo_io(ip)
        if country_code is None:
            return from_ip(ip, ipinfo=False)
        return get(country_code)
    else:
        return db_value_search(ip)

def from_ipnet(ipnet):
    ipnet = IPNetwork(ipnet)
    return db_value_search(ipnet)

def from_country(string):
    country = countries.lookup(string)
    return get(country)
